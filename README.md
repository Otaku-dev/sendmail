SendMail
Envío de correo electrónico  desde Termux.
*Created by: F@br1x and 你好😜*


Pasos de Instalación:

Actualizar Repositorios:

apt update && apt upgrade -y

Instalar Requisitos:

apt install pv figlet curl git -y

Clonar Repositorio:

git clone https://github.com/Fabr1x/sendmail

Seleccionar Directorio:

cd sendmail

Listamos:

ls

Damos permisos de ejecución:

chmod u+x sendmail.sh


Nota: Antes de ejecutar el script debe aceptar permisos al correo electrónico que usará ,el acceso de apps menos seguras, para que el envío de correo funcione correctamente sin ningún problema.
Puedes activarlo mediante este enlace: https://myaccount.google.com/u/1/lesssecureapps?pageId=none una vez aceptado los permisos ejecutamos el script:


bash sendmail.sh


Listo!!


*Created by: F@br1x and 你好😜*
